import React from 'react';
import Title from './Title';

export default class Header extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (e) => {
        const title = e.target.value;
        this.props.changeTitle(title);
    }

    render() {
        return (
            <div className="container">
                <Title title={this.props.title}/>
                <input type="text" value={this.props.title} onChange={this.handleChange}/>
            </div>
        );
    }
}