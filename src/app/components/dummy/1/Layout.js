import React from 'react';
import Header from './Header';
import Footer from './Footer';
import Button from './Button';

export default class Layout extends React.Component {
    constructor(props) {
        super(props);

        this.changeTitle = this.changeTitle.bind(this);
        this.displayConsole = this.displayConsole.bind(this);

        this.state = {
            title: 'Welcome'
        };
    }

    changeTitle = (title) => {
        this.setState({title});
    };

    displayConsole = () => {
        console.log('button clicked;');
    };

    render() {
        return (
            <div className="container">
                <Header title={this.state.title} changeTitle={this.changeTitle}/>
                <Footer text="Practice dummy application"/>
                <Button onClick={this.displayConsole}/>
            </div>
        );
    }
}