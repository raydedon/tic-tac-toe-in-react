import React from 'react';
import PropTypes from 'prop-types';
import './css-transitions.css';

const ExampleComponent = ({show, onClickHandler}) => {
    const componentClasses = ['example-component'];
    if (show) {
        componentClasses.push('show');
    } else {
        componentClasses.push('hide');
    }

    return (
        <div>
            <button onClick={onClickHandler}>check transition</button>
            <br/>
            <br/>
            <div className={componentClasses.join(' ')}></div>
        </div>
    );
};

ExampleComponent.propTypes = {
    show: PropTypes.bool.isRequired
};

export default ExampleComponent;