import React from 'react'
import { Transition, TransitionGroup } from 'react-transition-group'

const duration = 1000;

const defaultStyle = {
    transition: `${duration}ms ease-in-out`,
    padding: 20,
    display: 'inline-block',
    backgroundColor: '#8787d8',
};

const transitionStyles = {
    entering: {
        opacity: 0,
        transform: 'translateY(-100%)'
    },
    entered: {
        opacity: 1,
        transform: 'translateY(0)'
    },
    exiting: {
        opacity: 0,
        transform: 'translateY(-100%)'
    },
};

const Fade = ({ show: inProp,  onClickHandler}) => (
    <div>
        <button onClick={onClickHandler}>check transition</button>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <TransitionGroup className="fudu" component="div">
        <Transition in={inProp} timeout={duration} mountOnEnter={true} unmountOnExit={true} key={123}
                        onEnter={() => {console.info('in onEnter');}}
                        onEntering={() => {console.info('in onEntering');}}
                        onEntered={() => {console.info('in onEntered');}}
                        onExit={() => {console.info('in onExit');}}
                        onExiting={() => {console.info('in onExiting');}}
                        onExited={() => {console.info('in onExited');}}
            >
                {(state) => {
                    return (
                        <div style={{
                            ...defaultStyle,
                            ...transitionStyles[state]
                        }}>
                            I'm A fade Transition!
                        </div>
                    );
                }}
            </Transition>
        </TransitionGroup>
    </div>
);

export default Fade;
