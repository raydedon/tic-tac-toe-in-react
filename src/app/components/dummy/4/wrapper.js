import React, {Component} from 'react';
import ExampleComponent from './react-transitions';

export default class Wrapper extends Component {
    constructor(props) {
        super(props);
        this.state = {show: false};
        this.onClickHandler = this.onClickHandler.bind(this);
    }

    onClickHandler() {
        this.setState(({show}) => ({show: !show}));
    }

    render() {
        return (
            <ExampleComponent show={this.state.show} onClickHandler={this.onClickHandler}/>
        );
    }
}
