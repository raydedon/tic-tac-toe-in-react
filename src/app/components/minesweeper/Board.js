/**
 * Created by Ray on 12-01-2017
 */
import React from 'react';
import Cell from './Cell';
import {range} from 'ramda';

export default class Board extends React.Component {
    constructor(props) {
        super(props);
        this.getDistinctRandomWholeNumbers = this.getDistinctRandomWholeNumbers.bind(this);
        this.getNeighboringCells = this.getNeighboringCells.bind(this);
        this.assignMinesToCellsRandomly = this.assignMinesToCellsRandomly.bind(this);
        this.initializeTheBoard = this.initializeTheBoard.bind(this);
        this.changeValueForCell = this.changeValueForCell.bind(this);
        this.checkWeatherAllNonMineCellsAreOpened = this.checkWeatherAllNonMineCellsAreOpened.bind(this);
        this.openNeighborCells = this.openNeighborCells.bind(this);
        this.checkWeatherFlaggedCellsHaveMine = this.checkWeatherFlaggedCellsHaveMine.bind(this);
        this.callLeftClickOnNeighbor = this.callLeftClickOnNeighbor.bind(this);
        this.setMineFlag = this.setMineFlag.bind(this);
        this.renderCell = this.renderCell.bind(this);

        this.stack = [];
        this.state = {matrixBoard: this.initializeTheBoard.call(this, props)};
    }

    getDistinctRandomWholeNumbers(totalDistinctNumbers, max, excludeFirstClickCellPosition) {
        let set = [];
        if (excludeFirstClickCellPosition !== undefined) {
            set.push(excludeFirstClickCellPosition);
        }
        for(let i=0; i < totalDistinctNumbers; i++) {
            let randomNum = Math.floor(Math.random() * max);
            while(set.indexOf(randomNum) > -1) {
                randomNum = Math.floor(Math.random() * max);
            }
            set.push(randomNum);
        }
        if (excludeFirstClickCellPosition !== undefined) {
            set.shift();
        }
        return set;
    }

    getNeighboringCells({x, y}, matrixBoard, totalCols, totalRows) {
        let set = [];
        if(x > 0 && y > 0) {
            set.push(matrixBoard[y-1][x-1]);
        }
        if(y > 0) {
            set.push(matrixBoard[y-1][x]);
            if(x < totalCols - 1) {
                set.push(matrixBoard[y-1][x+1]);
            }
        }
        if(x > 0) {
            set.push(matrixBoard[y][x-1]);
            if(y < totalRows - 1) {
                set.push(matrixBoard[y+1][x-1]);
            }
        }
        if(x < totalCols - 1) {
            set.push(matrixBoard[y][x+1]);
        }
        if(y < totalRows - 1) {
            set.push(matrixBoard[y+1][x]);
        }
        if(x < totalCols - 1 && y < totalRows - 1) {
            set.push(matrixBoard[y+1][x+1]);
        }
        return set;
    }

    assignMinesToCellsRandomly(excludeFirstClickCellPosition) {
        const totalRows = this.props.rows;
        const totalCols = this.props.columns;
        const matrixBoard = this.state.matrixBoard;

        let set = this.getDistinctRandomWholeNumbers(this.props.totalMinesPlanned, totalCols*totalRows, excludeFirstClickCellPosition);
        /*
         let set = [9, 14, 18, 21, 22, 24, 28, 36, 46, 63];
         */
        for(let i=0; i < this.props.totalMinesPlanned; i++) {
            let x = parseInt(set[i]%totalCols);
            let y = parseInt(set[i]/totalCols);
            matrixBoard[y][x].hasMine = true;
        }
        for(let i=0; i < this.props.totalMinesPlanned; i++) {
            let x = parseInt(set[i]%totalCols);
            let y = parseInt(set[i]/totalCols);
            this.getNeighboringCells(matrixBoard[y][x], matrixBoard, totalCols, totalRows)
                .filter(cell => !cell.hasMine)
                .map(({x, y}) => matrixBoard[y][x].neighborMineCount++);
        }
        this.setState({matrixBoard});
    }

    initializeTheBoard({rows, columns}) {
        let matrixBoard = [];
        range(0, rows).map(j => {
            matrixBoard[j] = [];
            range(0, columns).map(i => {
                let key = j*parseFloat(columns) + i;
                matrixBoard[j][i] = {
                    x: i,
                    y: j,
                    isOpened: false,
                    hasFlag: false,
                    hasMine: false,
                    neighborMineCount: 0,
                    clickedOnMine: false,
                    key: key,
                    id: key,
                    noMine: false
                };
            });
        });
        return matrixBoard;
    }

    changeValueForCell(cell) {
        let {x, y} = cell;
        let matrixBoard = this.state.matrixBoard;
        let matrixBoardCell = matrixBoard[y][x];
        if (!matrixBoardCell.isOpened) {
            // if cell is closed
            if (matrixBoardCell.hasFlag) {
                return false;
            } else if(matrixBoardCell.hasMine) {
                // if the cell was closed and the cell had mine then game over, show the bomb.
                matrixBoardCell.isOpened = true;
                matrixBoardCell.clickedOnMine = true;
                this.setState({matrixBoard});
                this.props.setGameOverByLoosing();
            } else if(!matrixBoardCell.hasMine) {
                if(matrixBoardCell.neighborMineCount > 0) {
                    // If cell has neighborMineCount > 0, set isOpened=true, show neighborMineCount on the cell.
                    matrixBoardCell.isOpened = true;
                    this.setState({matrixBoard});
                } else if(matrixBoardCell.neighborMineCount === 0) {
                    // If cell has neighborMineCount == 0, set isOpened=true, call Left Click on a Cell for all neighbor cells, which hasFlag=false and isOpened=false
                    this.stack.push(cell);
                    this.callLeftClickOnNeighbor.call(this, 1);
                }
                // check weather all the non mine cells are open. if so display you have won to the user
                if (this.checkWeatherAllNonMineCellsAreOpened.call(this, this.state.matrixBoard))
                {
                    this.props.endGameWithSuccess();
                }
            }
        } else {
            // open the neighbour cells if the number of mines has been marked.
            let flaggedNeighBors = this.getNeighboringCells({x, y}, matrixBoard, this.props.columns, this.props.rows).filter(cell => cell.hasFlag);
            if(matrixBoardCell.neighborMineCount > 0 && matrixBoardCell.neighborMineCount === flaggedNeighBors.length) {
                this.openNeighborCells(matrixBoardCell);
            }
        }
    }

    checkWeatherAllNonMineCellsAreOpened(matrixBoard) {
        if (matrixBoard.reduce((prev, next) => prev.concat(next))
                .filter(cell => cell.isOpened && !cell.hasMine).length ===
            (this.props.columns*this.props.rows - this.props.totalMinesPlanned))
        {
            return true;
        }
        return false;
    }

    openNeighborCells(cell) {
        const totalRows = this.props.rows;
        const totalCols = this.props.columns;
        const matrixBoard = this.state.matrixBoard;
        const x = parseInt(cell.id%totalCols);
        const y = parseInt(cell.id/totalCols);
        let neighborCells = this.getNeighboringCells(cell, matrixBoard, this.props.columns, this.props.rows);
        let flaggedNeighBors = neighborCells.filter(cell => cell.hasFlag);
        if(this.checkWeatherFlaggedCellsHaveMine.call(this, flaggedNeighBors)) {
            this.stack.push(cell);
            this.callLeftClickOnNeighbor.call(this, 1);
            // check weather all the non mine cells are open. if so display you have won to the user
            if (this.checkWeatherAllNonMineCellsAreOpened.call(this, this.state.matrixBoard))
            {
                this.props.endGameWithSuccess();
            }
        } else {
            neighborCells.map(({hasFlag, hasMine, isOpened, x, y}) => {
                if (hasFlag && !hasMine && !isOpened) {
                    matrixBoard[y][x].noMine = true;
                    matrixBoard[y][x].hasMine = false;
                    matrixBoard[y][x].hasFlag = false;
                } else if (hasMine && !isOpened) {
                    matrixBoard[y][x].clickedOnMine = true;
                }
                matrixBoard[y][x].isOpened = true;
            });
            // game over
            this.setState({matrixBoard});
            this.props.setGameOverByLoosing();
        }
    }

    checkWeatherFlaggedCellsHaveMine(flaggedNeighBors) {
        for(let i=0; i< flaggedNeighBors.length; i++) {
            if(!flaggedNeighBors.pop().hasMine) {
                return false;
            }
        }
        return true;
    }

    callLeftClickOnNeighbor(numberOfItr) {
        const totalRows = this.props.rows;
        const totalCols = this.props.columns;
        for(let i = 0; i< numberOfItr; i++) {
            let {x, y} = this.stack.pop();
            let {matrixBoard} = this.state;
            matrixBoard[y][x].isOpened = true;
            this.setState({matrixBoard});
            let numberOfNewCellsPushedOnStack = 0;
            this.getNeighboringCells({x, y}, matrixBoard, totalCols, totalRows)
                .filter(({hasFlag, isOpened}) => !hasFlag && !isOpened)
                .map(cell => {
                    if (cell.neighborMineCount === 0 && this.stack.map(cell => cell.id).indexOf(cell.id) === -1) {
                        this.stack.push(cell);
                        numberOfNewCellsPushedOnStack++;
                    } else {
                        cell.isOpened = true;
                    }
                });
            this.callLeftClickOnNeighbor(numberOfNewCellsPushedOnStack);
        }
    }

    setMineFlag({x, y}) {
        this.setState((prevState, props) => {
            let {matrixBoard} = prevState;
            matrixBoard[y][x].hasFlag = !matrixBoard[y][x].hasFlag;
            if (matrixBoard[y][x].hasFlag) {
                this.props.changeTotalFlagSet(1);
            } else {
                this.props.changeTotalFlagSet(-1);
            }
            return {matrixBoard};
        });
    }

    renderCell(x, y) {
        return <Cell {...this.state.matrixBoard[y][x]}
                     changeValueForCell={this.changeValueForCell}
                     setMineFlag={this.setMineFlag}
                     gameStatus={this.props.gameStatus}
                     startTheGame={() => this.props.startTheGame()}
                     assignMinesToCellsRandomly={this.assignMinesToCellsRandomly}/>;
    }

    render() {
        let mineSweeperRows = [];
        for(let i=0; i < this.props.rows; i++) {
            let mineSweeperCells = [];
            for(let j=0; j < this.props.columns; j++) {
                mineSweeperCells.push(this.renderCell(j, i));
            }
            mineSweeperRows.push(<div key={`row-${i}`} className="row">{mineSweeperCells}</div>);
        }
        return <div className="text-center panel-body matrixBoard">{mineSweeperRows}</div>;
    }
}