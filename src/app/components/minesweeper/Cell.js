/**
 * Created by Ray on 12-01-2017.
 */
import React from 'react';
import GAME from './Util';

export default class Cell extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.processClasses = this.processClasses.bind(this);
    }

    handleClick(e) {
        if (this.props.gameStatus === GAME.OVER_BY_LOOSING || this.props.gameStatus === GAME.OVER_BY_WINNING) {
            e.preventDefault();
            e.stopPropagation();
            return false;
        } else if (this.props.gameStatus === GAME.NOT_YET_STARTED) {
            if (e.button === 2) {
                this.props.assignMinesToCellsRandomly();
            } else {
                this.props.assignMinesToCellsRandomly(this.props.id);
            }
            this.props.startTheGame();
        }

        if (e.button === 2) {
            if(!this.props.isOpened) {
                // if cell is closed, then show flag
                this.props.setMineFlag(this.props);
            }
            e.preventDefault();
            e.stopPropagation();
            return false;
        } else {
            this.props.changeValueForCell(this.props);
        }
    }

    processClasses() {
        let classSet = new Set();
        classSet.add('square');
        classSet.add('btn');
        classSet.add('btn-default');
        classSet.add('mine-sweeper-cell');
        if (this.props.hasMine) {
            if (this.props.isOpened) {
                classSet.add('fa');
                classSet.add('fa-bomb');
                if (this.props.clickedOnMine) {
                    classSet.add('background-red');
                }
            } else {
                if (this.props.hasFlag) {
                    classSet.add('fa');
                    classSet.add('fa-flag-checkered');
                } else if (this.props.gameStatus === GAME.OVER_BY_WINNING) {
                    classSet.add('fa');
                    classSet.add('fa-flag-checkered');
                } else if (this.props.gameStatus === GAME.OVER_BY_LOOSING) {
                    classSet.add('fa');
                    classSet.add('fa-bomb');
                    classSet.add('force-open');
                    classSet.add('game-over');
                }
            }
        } else if (this.props.noMine) {
            classSet.add('fa');
            classSet.add('fa-ban');
            classSet.add('text-danger');
        }

        if (!this.props.isOpened) {
            classSet.add('closed-box');
        }

        if (this.props.hasFlag) {
            classSet.add('fa');
            classSet.add('fa-flag-checkered');
        }
        return Array.from(classSet).join(" ")
    }

    render() {
        return (
            <button className={this.processClasses()}
                    onClick={this.handleClick}
                    onContextMenu={this.handleClick}
                    id={this.props.id}>{this.props.neighborMineCount > 0 && this.props.isOpened && !this.props.noMine ? this.props.neighborMineCount : ''}</button>
        );
    }
}