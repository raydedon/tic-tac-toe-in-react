/**
 * Created by Ray on 4/6/2017
 */
import React from 'react';
import Modal from '../../../../node_modules/react-bootstrap/lib/Modal';
import Button from '../../../../node_modules/react-bootstrap/lib/Button';

export default class CustomizeBoardModal extends React.Component {
    constructor(props) {
        super(props);
        this.changeRows = this.changeRows.bind(this);
        this.changeColumns = this.changeColumns.bind(this);
        this.changeTotalMinesPlanned = this.changeTotalMinesPlanned.bind(this);
        this.changeOfDifficultyLevel = this.changeOfDifficultyLevel.bind(this);
        this.setDifficultyLevelToCustom = this.setDifficultyLevelToCustom.bind(this);
        this.submitDifficultyLevelForm = this.submitDifficultyLevelForm.bind(this);

        this.state = {
            difficultyLevelStr: this.props.difficultyLevelStr,
            gameLevels: this.props.gameLevels
        };
    }

    changeRows(e) {
        let value = e.target.value;
        this.setState((prevState, props) => {
            let tempObj = Object.assign({}, prevState.gameLevels);
            tempObj.CUSTOM.rows = value;
            return {gameLevels: tempObj};
        });
    }

    changeColumns(e) {
        let value = e.target.value;
        this.setState((prevState, props) => {
            let tempObj = Object.assign({}, prevState.gameLevels);
            tempObj.CUSTOM.columns = value;
            return {gameLevels: tempObj};
        });
    }

    changeTotalMinesPlanned(e) {
        let value = e.target.value;
        this.setState((prevState, props) => {
            let tempObj = Object.assign({}, prevState.gameLevels);
            tempObj.CUSTOM.totalMinesPlanned = value;
            return {gameLevels: tempObj};
        });
    }

    changeOfDifficultyLevel(e) {
        this.state.difficultyLevelStr = e.target.value;
    }

    setDifficultyLevelToCustom(e) {
        this.customRadioInputField.checked = true;
        this.setState({difficultyLevelStr: 'CUSTOM'});
    }

    submitDifficultyLevelForm() {
        this.props.onChangeOfDifficultyLevel(this.state);
        this.props.onHide();
    }

    render() {
        return (
            <Modal {...this.props} bsSize="small">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">Game Options</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <table cellSpacing="0" cellPadding="2" id="options" className="dialog" style={{display: 'table'}}>
                        <colgroup>
                            <col width="40%"/>
                            <col width="10%"/>
                            <col width="10%"/>
                            <col width="10%"/>
                        </colgroup>
                        <tbody>
                        <tr className="dialog-bar">
                            <td></td>
                            <td>Height</td>
                            <td>Width</td>
                            <td>Mines</td>
                        </tr>
                        <tr>
                            <td>
                                <label><input type="radio"
                                              name="field"
                                              id="beginner"
                                              onChange={this.changeOfDifficultyLevel}
                                              value="BEGGINER"
                                              defaultChecked={this.props.difficultyLevelStr ==='BEGGINER'}/>&nbsp;<strong>Beginner</strong></label>
                            </td>
                            <td>8</td>
                            <td>8</td>
                            <td>10</td>
                        </tr>
                        <tr>
                            <td>
                                <label><input type="radio"
                                              name="field"
                                              id="intermediate"
                                              onChange={this.changeOfDifficultyLevel}
                                              value="INTERMEDIATE"
                                              defaultChecked={this.props.difficultyLevelStr ==='INTERMEDIATE'}/>&nbsp;<strong>Intermediate</strong></label>
                            </td>
                            <td>16</td>
                            <td>16</td>
                            <td>40</td>
                        </tr>
                        <tr>
                            <td>
                                <label><input type="radio"
                                              name="field"
                                              id="expert"
                                              onChange={this.changeOfDifficultyLevel}
                                              value="EXPERT"
                                              defaultChecked={this.props.difficultyLevelStr ==='EXPERT'}/>&nbsp;<strong>Expert</strong></label>
                            </td>
                            <td>16</td>
                            <td>30</td>
                            <td>99</td>
                        </tr>
                        <tr>
                            <td>
                                <label><input type="radio"
                                              name="field"
                                              id="custom"
                                              onChange={this.changeOfDifficultyLevel}
                                              value="CUSTOM"
                                              defaultChecked={this.props.difficultyLevelStr ==='CUSTOM'}
                                              ref={(input) => { this.customRadioInputField = input; }}/>&nbsp;<strong>Custom</strong></label>
                            </td>
                            <td><input type="text"
                                       value={this.state.gameLevels.CUSTOM.rows}
                                       key="custom_height"
                                       className="form-control input-sm"
                                       onChange={this.changeRows}
                                       onFocus={this.setDifficultyLevelToCustom}/></td>
                            <td><input type="text"
                                       value={this.state.gameLevels.CUSTOM.columns}
                                       key="custom_width"
                                       className="form-control input-sm"
                                       onChange={this.changeColumns}
                                       onFocus={this.setDifficultyLevelToCustom}/></td>
                            <td><input type="text"
                                       value={this.state.gameLevels.CUSTOM.totalMinesPlanned}
                                       key="custom_mines"
                                       className="form-control input-sm"
                                       onChange={this.changeTotalMinesPlanned}
                                       onFocus={this.setDifficultyLevelToCustom}/></td>
                        </tr>
                        </tbody>
                    </table>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.submitDifficultyLevelForm}>Submit</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}