/**
 * Created by Ray on 12-01-2017.
 */
import React from 'react';
import Board from './Board';
import Timer from './Timer';
import Button from '../../../../node_modules/react-bootstrap/lib/Button';
import CustomizeBoardModal from './CustomizeBoardModal';
import GAME from './Util';

export default class Game extends React.Component {
    constructor(props) {
        super(props);
        this.setGameOverByLoosing = this.setGameOverByLoosing.bind(this);
        this.changeTotalFlagSet = this.changeTotalFlagSet.bind(this);
        this.endGameWithSuccess = this.endGameWithSuccess.bind(this);
        this.resetGame = this.resetGame.bind(this);
        this.changeOfDifficultyLevelAndReset = this.changeOfDifficultyLevelAndReset.bind(this);
        this.getBtnClass = this.getBtnClass.bind(this);

        this.state = {
            realTimeInfo : '',
            totalFlagsSet: 0,
            gameStatus: GAME.NOT_YET_STARTED,
            currentTime: new Date().toLocaleTimeString(),
            showModal: false,
            difficultyLevelStr: 'BEGGINER',
            gameLevels: {
                BEGGINER: {rows: 8, columns: 8, totalMinesPlanned: 10},
                INTERMEDIATE: {rows: 16, columns: 16, totalMinesPlanned: 40},
                EXPERT: {rows: 16, columns: 30, totalMinesPlanned: 99},
                CUSTOM: {rows: 20, columns: 30, totalMinesPlanned: 145}
            }
        };
    }

    resetGame() {
        this.setState({
            realTimeInfo : '',
            totalFlagsSet: 0,
            gameStatus: GAME.NOT_YET_STARTED,
            currentTime: new Date().toLocaleTimeString()
        });
    }

    setGameOverByLoosing(e) {
        this.setState({
            gameStatus: GAME.OVER_BY_LOOSING,
            realTimeInfo: 'you have lost the game'
        });
    }

    changeTotalFlagSet(step) {
        this.setState((prevState, props) => ({
            totalFlagsSet: prevState.totalFlagsSet + step
        }));
    }

    endGameWithSuccess() {
        this.setState({
            gameStatus: GAME.OVER_BY_WINNING,
            realTimeInfo: 'you have successfully won the game'
        });
    }

    changeOfDifficultyLevelAndReset(difficultyLevelObjArg) {
        this.setState(Object.assign({}, difficultyLevelObjArg, {
            realTimeInfo : '',
            totalFlagsSet: 0,
            gameStatus: GAME.NOT_YET_STARTED,
            currentTime: new Date().toLocaleTimeString()
        }));
    }

    getBtnClass() {
        let btnClass = 'square reset-btn';
        if (this.state.gameStatus === GAME.OVER_BY_WINNING) {
            btnClass += ' icomoon icon-cool';
        } else if (this.state.gameStatus === GAME.OVER_BY_LOOSING) {
            btnClass += ' icomoon icon-sad';
        } else {
            btnClass += ' icomoon icon-happy';
        }
        return btnClass
    }

    render() {
        let step = this.state.gameStatus === GAME.STARTED ? 1 : 0;
        return (
            <div>
                <Button bsStyle="default" bsSize="xsmall" onClick={()=>this.setState({ showModal: true })}><i className="fa fa-wrench"></i></Button>
                <CustomizeBoardModal show={this.state.showModal}
                                     onHide={()=>this.setState({ showModal: false })}
                                     onChangeOfDifficultyLevel={this.changeOfDifficultyLevelAndReset}
                                     difficultyLevelStr={this.state.difficultyLevelStr}
                                     gameLevels={this.state.gameLevels}/>
                <div className="col-sm-offset-3 col-sm-2 text-right">
                    {this.state.gameLevels[this.state.difficultyLevelStr].totalMinesPlanned - this.state.totalFlagsSet}
                </div>
                <div className="col-sm-2 text-center">
                    <button className={this.getBtnClass()} onClick={this.resetGame}></button>
                </div>
                <div className="col-sm-2 text-left"><Timer step={step} key={this.state.currentTime}/></div>
                <div className="col-sm-12">
                    <Board key={this.state.currentTime}
                           {...this.state.gameLevels[this.state.difficultyLevelStr]}
                           onClick={this.handleClick}
                           onContextMenu={this.handleClick}
                           setGameOverByLoosing={this.setGameOverByLoosing}
                           changeTotalFlagSet={this.changeTotalFlagSet}
                           gameStatus={this.state.gameStatus}
                           endGameWithSuccess={this.endGameWithSuccess}
                           startTheGame={() => this.setState({gameStatus: GAME.STARTED})}/>;
                </div>
                <div className="col-sm-12 text-center">{this.state.realTimeInfo}</div>
            </div>
        );
    }
}