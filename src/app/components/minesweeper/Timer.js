/**
 * Created by Ray on 4/5/2017.
 */
import React from 'react';

export default class Timer extends React.Component {
    constructor(props) {
        super(props);
        this.tick = this.tick.bind(this);

        this.state = {timeSpentInSec: 0};
    }

    componentDidMount() {
        console.info('in componentDidMount');
        this.timerID = setInterval(this.tick, 1000);
    }

    componentWillUnmount() {
        console.info('in componentWillUnmount');
        clearInterval(this.timerID);
    }

    tick() {
        this.setState((prevState, props) => ({
            timeSpentInSec: prevState.timeSpentInSec + props.step
        }));
    }

    render() {
        return (<span>{this.state.timeSpentInSec}</span>)
    }
}
