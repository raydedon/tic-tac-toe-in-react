/**
 * Created by Ray on 5/31/2017.
 */
module.exports = {
    NOT_YET_STARTED: 0,
    STARTED: 1,
    OVER_BY_LOOSING: 2,
    OVER_BY_WINNING: 3
};