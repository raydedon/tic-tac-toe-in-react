import React from 'react';
import Square from './Square';

const player = {
	0: 'O',
	1: 'X',
};

export default class Board extends React.Component {
	constructor(props) {
		super(props);
		this.state = {matrixBoard : []};
		this.changeValueForSquare = this.changeValueForSquare.bind(this);
		this.getNextPlayer = this.getNextPlayer.bind(this);
		this.calculateWinner = this.calculateWinner.bind(this);
	}
	
	getNextPlayer() {
		console.info(`in board getNextPlayer ${Object.keys(this.state.matrixBoard).length}`);
		return Object.keys(this.state.matrixBoard).length%2;
	}
	
	calculateWinner(matrixBoard) {
		const lines = [
				[0, 1, 2],
				[3, 4, 5],
				[6, 7, 8],
				[0, 3, 6],
				[1, 4, 7],
				[2, 5, 8],
				[0, 4, 8],
				[2, 4, 6],
			];
		for (let i = 0; i < lines.length; i++) {
			const [a, b, c] = lines[i];
			if (matrixBoard[a] !== undefined && matrixBoard[a] === matrixBoard[b] && matrixBoard[a] === matrixBoard[c]) {
				return matrixBoard[a];
			}
		}
	}

	changeValueForSquare(index) {
		const matrixBoard = this.state.matrixBoard;
		if (this.calculateWinner(matrixBoard) != undefined) {
			return;
		}
		matrixBoard[index] = this.getNextPlayer();
		this.setState({'matrixBoard': matrixBoard});
		this.props.updateInfo(matrixBoard);
	}
	
	renderSquare(i) {
		return <Square key={i} changeValueForThisSquare={this.changeValueForSquare.bind(this, i)} value={this.state.matrixBoard[i]}/>;
	}
	
	render() {
		return (
			<div className="text-center panel-body matrixBoard">
				<div className="row">
					{this.renderSquare(0)}
					{this.renderSquare(1)}
					{this.renderSquare(2)}
				</div>
				<div className="row">
					{this.renderSquare(3)}
					{this.renderSquare(4)}
					{this.renderSquare(5)}
				</div>
				<div className="row">
					{this.renderSquare(6)}
					{this.renderSquare(7)}
					{this.renderSquare(8)}
				</div>
			</div>
		);
	}
}