import React from 'react';
import Board from './Board';

const player = {
	0: 'O',
	1: 'X',
};

export default class Game extends React.Component {
	constructor(props) {
		super(props);
		this.state = {realTimeInfo : `Player ${player[0]}'s move`, moves: []};
		this.updateGameInfo = this.updateGameInfo.bind(this);
		this.calculateWinner = this.calculateWinner.bind(this);
	}

	calculateWinner(matrixBoard) {
		const lines = [
				[0, 1, 2],
				[3, 4, 5],
				[6, 7, 8],
				[0, 3, 6],
				[1, 4, 7],
				[2, 5, 8],
				[0, 4, 8],
				[2, 4, 6],
			];
		for (let i = 0; i < lines.length; i++) {
			const [a, b, c] = lines[i];
			if (matrixBoard[a] !== undefined && matrixBoard[a] === matrixBoard[b] && matrixBoard[a] === matrixBoard[c]) {
				return matrixBoard[a];
			}
		}
	}
	
	getNextPlayer(matrixBoard) {
		console.info(`in games getNextPlayer ${Object.keys(matrixBoard).length}`);
		return Object.keys(matrixBoard).length%2;
	}

	updateGameInfo(matrixBoard) {
		let playerIndex = this.calculateWinner(matrixBoard);
		if(playerIndex != undefined) {
			this.setState({realTimeInfo: `Player ${player[playerIndex]} has won`});
		} else if (Object.keys(matrixBoard).length < 9) {
			this.setState({realTimeInfo: `Player ${player[this.getNextPlayer(matrixBoard)]}'s move`});
		} else {
			this.setState({realTimeInfo: 'Game Over'});
		}
	}
	
	render() {
		return (
			<div className="media">
				<div className="media-left media-middle">
					<Board updateInfo={this.updateGameInfo} />
				</div>
				<div className="media-body">
					<h4 className="media-heading">{this.state.realTimeInfo}</h4>
					<div>
						<ul>
						{this.state.moves.map(function(listValue){
							return <li>{listValue}</li>;
						})}
						</ul>
					</div>
				</div>
			</div>
		);
	}
}