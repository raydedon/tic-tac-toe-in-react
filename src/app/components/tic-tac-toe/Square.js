import React from 'react';

const player = {
	0: 'O',
	1: 'X',
};

export default class Square extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.handleClick = this.handleClick.bind(this);
	}
	
	handleClick(e) {
		if (this.props.value != undefined) {
			e.preventDefault();
			e.stopPropagation();
			return false;
		} else {
			this.props.changeValueForThisSquare();
		}
	}
	
	render() {
		let className = 'square btn btn-default ';
		className += this.props.value in player ? ('fa fa-player-' + player[this.props.value]) : '';
		return <button className={className} onClick={this.handleClick}></button>;
	}
}