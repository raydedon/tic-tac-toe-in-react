import React from 'react';
import ReactDOM from 'react-dom';
import GameMinesweeper from './components/minesweeper/Game';
import Clock from './components/dummy/3/Tick';
import ToDoList from './components/dummy/4/todolist';
import Wrapper from './components/dummy/4/wrapper';
import Example from './components/dummy/4/react-transitions';
import '../stylesheets/style.scss';


ReactDOM.render(<Wrapper/>, document.getElementById('app'));