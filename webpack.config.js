const Webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const pathsToClean = ['public'];
const cleanOptions = {
    root:     path.resolve(__dirname),
    verbose:  true,
    dry:      false
};

const bootstrapEntryPoints = require('./webpack.bootstrap.config');
const devSCSS = ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'];
const prodSCSS = ExtractTextPlugin.extract({
    fallback: 'style-loader',
    //resolve-url-loader may be chained before sass-loader if necessary
    use: ['css-loader', 'postcss-loader', 'sass-loader']
});
const isProd = (process.env.NODE_ENV || 'dev') === 'prod';
const scssConfig = isProd ? prodSCSS : devSCSS;
const devCSS = ['style-loader', 'css-loader', 'postcss-loader'];
const prodCSS = ExtractTextPlugin.extract({
    fallback: 'style-loader',
    //resolve-url-loader may be chained before sass-loader if necessary
    use: ['css-loader', 'postcss-loader']
});
const cssConfig = isProd ? prodCSS : devCSS;
const bootstrapConfig = isProd ? bootstrapEntryPoints.prod : bootstrapEntryPoints.dev;
module.exports = {
    context: path.resolve(__dirname, 'public'),
    devtool: 'cheap-module-eval-source-map',
    entry: {
        main: '../src/app/main.js',
        bootstrap: bootstrapConfig
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: '[name].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js?/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: cssConfig
            },
            {
                test: /\.scss$/,
                use: scssConfig
            },
            {
                test: /\.(png|jpg|gif|ttf|eot|woff2?|svg)$/,
                use: [{
                    loader: 'url-loader'
                }]
            },
            {
                test: /\.html$/,
                use: 'html-loader',
                exclude: [
                    /(node_modules|public)/,
                    path.resolve(__dirname, 'src/index.html')
                ]
            },
            {
                test: /bootstrap-sass\/assets\/javascripts\//,
                use: 'imports-loader?jQuery=jquery'
            },
        ]
    },
    devServer: {
        contentBase: path.join(__dirname, "public"),
        port: 8002,
        host: 'in-sattur',
        hot: true,
        compress: true,
        inline: true,
        open: true
    },
    plugins: [
        new CleanWebpackPlugin(pathsToClean, cleanOptions),
        new ExtractTextPlugin({
            filename: '[name].css',
            disable: !isProd,
            allChunks: true
        }),
        new HtmlWebpackPlugin({
            title: 'React App',
            template: '../src/index.html',
            hash: true,
            cache: true
        }),
        new Webpack.NamedModulesPlugin(),
        new Webpack.HotModuleReplacementPlugin(),
        new Webpack.ProvidePlugin({
            jQuery: 'jquery'
        })
    ]
};